Implemented Project Lombok
Containerised the application functionality and data storage
Attempted multistaging to reduce file size. However our jar file size is only 20+MB. So it is not necessary. 

Didn't get to run the application in OpenShift and explore AWS storage. 

The work above was completed as a team over screen sharing. 