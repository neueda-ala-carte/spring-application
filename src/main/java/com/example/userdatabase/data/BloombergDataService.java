package com.example.userdatabase.data;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;


/**
 * Data service for accessing Bloomberg
 * @author anna
 */
@Service
@Lazy
public class BloombergDataService {

    @Value("${rapidapi.apikey}")
    private String API_KEY;

    @Value("${rapidapi.host.bloomberg}")
    private String HOST;

    public JSONObject getBloombergNewsData() {
        String url = String.format("https://%s/news/list?id=markets", HOST);
        return getData(url);
    }

    private JSONObject getData(String url) {
        JSONObject res = new JSONObject();
        try {
            HttpResponse<JsonNode> response = Unirest.get(url)
                    .header("x-rapidapi-host", HOST)
                    .header("x-rapidapi-key", API_KEY)
                    .asJson();
            res = response.getBody().getObject();
        } catch (UnirestException ue) {
            System.out.println(ue.getMessage());
        }
        return res;
    }
}
