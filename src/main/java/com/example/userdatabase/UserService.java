package com.example.userdatabase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    //Create user
    public User create(String name, String email, String number){
        return userRepository.save(new User(name, email, number));
    }

    public User create(User user){
        return user;
    }

    //Retrieve all users
    public List<User> getAll(){
        return userRepository.findAll();
    }

    //Retrieve user by number
    public User getByNumber(String number){
        return userRepository.findByNumber(number);
    }

    //Update operation
    public User update(String name, String email, String number){
        User user = userRepository.findByName(name);
        user.setEmail(email);
        user.setNumber(number);
        return userRepository.save(user);
    }

    //Delete all users
    public void deleteAll(){
        userRepository.deleteAll();
    }

    //Delete a user
    public void delete(String number){
        User user = userRepository.findByNumber(number);
        userRepository.delete(user);
    }

    /**
     * Authentication for market data access
     * @param apiKey
     * @return boolean
     */
    public boolean authenticateApiKey(String apiKey) {
        List<User> userList = userRepository.findAll();
        for(User user: userList){
            if(user.getApiKey().equals(apiKey)){
                return true;
            }
        }
        return false;
    }
}
